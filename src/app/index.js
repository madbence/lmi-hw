import Inferno from 'inferno';
import Component from 'inferno-component';
import {Link} from 'inferno-router';
import {connect} from 'inferno-redux';

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      term: this.props.term,
    };
  }

  componentWillReceiveProps(next) {
    if (next.term === this.props.term || !next.term) return;
    this.setState({term: next.term});
    this.search(next.term);
  }

  search(term) {
    this.context.router.push(`/?q=${term}`);
  }

  render() {
    const term = this.state.term == null ? 'javascript' : this.state.term;
    const update = e => this.setState({term: e.currentTarget.value});
    const keyDown = e => {
      if (e.keyCode === 13) this.search(term);
    };
    return (
      <div className='app'>
        <div className='header'>
          <div className='header--cart-container'>
            <Link to='/cart'>Cart{this.props.cartSize ? ` (${this.props.cartSize})` : ''}</Link>
          </div>
          <div className='header--search-container'>
            <input className='header--search' value={term} onInput={update} onKeyDown={keyDown} />
            <button className='header--search-button' onClick={() => this.search(term)}>Search</button>
          </div>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default connect(state => ({
  cartSize: state.cart.length,
}))(App);
