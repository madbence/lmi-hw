import {createStore} from 'redux';
import bookReducer from './book/reducer';
import cartReducer from './cart/reducer';

export default createStore((state, action) => [bookReducer, cartReducer].reduce((state, reducer) => reducer(state, action), state), {
  books: {
    byId: {},
  },
  cart: JSON.parse(window.localStorage.getItem('cart') || '[]'),
});
