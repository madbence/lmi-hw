export const RECEIVE = 'book:receive';
export const CLEAR = 'book:clear';

export const receive = books => ({
  type: RECEIVE,
  payload: books,
});

export const clear = () => ({
  type: CLEAR,
});
