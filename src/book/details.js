import Inferno from 'inferno';
import Component from 'inferno-component';
import {connect} from 'inferno-redux';

import * as books from './api';
import {receive} from './actions';

import * as cart from '../cart/api';
import {add, remove} from '../cart/actions';

class BookDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: !props.book,
    };
  }

  async componentDidMount() {
    if (this.props.book) return;
    this.setState({loading: true});
    await this.props.load(this.props.id);
    this.setState({loading: false});
  }

  render() {
    const book = this.props.book;
    if (this.state.loading) {
      return (
        <div class='book-details'>
          <h1>Loading...</h1>
        </div>
      );
    }

    if (!book) {
      return (
        <div class='book-details'>
          <h1>Book not found...</h1>
        </div>
      );
    }

    return (
      <div class='book-details'>
        <h1>{book.title}</h1>
        <img className='book-details--thumbnail' src={book.thumbnail} alt={book.title} />
        {book.authors.length > 0 && <h2>by {book.authors.join(', ')}</h2>}
        <div className='book-details--description' dangerouslySetInnerHTML={ {__html: book.description}} />
        {
          this.props.added
            ? <button onClick={() => this.props.remove(this.props.id)}>remove</button>
            : <button onClick={() => this.props.add(this.props.id)}>add</button>
        }
      </div>
    );
  }
}

export default connect((state, props) => ({
  book: state.books.byId[props.id],
  added: state.cart.includes(props.id),
}), dispatch => ({
  async load(id) {
    const book = await books.getById(id);
    if (!book) return;
    dispatch(receive([book]));
  },
  async add(id) {
    await cart.add(id);
    dispatch(add(id));
  },
  async remove(id) {
    await cart.remove(id);
    dispatch(remove(id));
  },
}))(BookDetails);
