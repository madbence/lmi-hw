import {RECEIVE, CLEAR} from './actions';

export default (state, action) => {
  switch (action.type) {
    case RECEIVE:
      return {
        ...state,
        books: {
          byId: action.payload.reduce((byId, book) => {
            byId[book.id] = book;
            return byId;
          }, {...state.books.byId}),
        },
        searching: false,
      };
    case CLEAR:
      return {
        ...state,
        books: {
          byId: {},
        },
      };
    default:
      return state;
  }
};
