// @flow
import qs from 'querystring';
import {fetch} from '../utils';

type Book = {
  id: number,
  title: string,
};

const parse = volume => ({
  id: volume.id,
  title: volume.volumeInfo.title,
  subtitle: volume.volumeInfo.subtitle,
  authors: volume.volumeInfo.authors || [],
  publisher: volume.volumeInfo.publisher,
  publishDate: volume.volumeInfo.publishedDate,
  pages: volume.volumeInfo.pageCount,
  thumbnail: volume.volumeInfo.imageLinks && volume.volumeInfo.imageLinks.thumbnail,
  price: volume.saleInfo.listPrice && volume.saleInfo.listPrice.amount,
  currency: volume.saleInfo.listPrice && volume.saleInfo.listPrice.currencyCode,
  description: volume.volumeInfo.description,
});

export async function search(term: string, offset: number = 0): Promise<Book[]> {
  const res = await fetch('https://www.googleapis.com/books/v1/volumes?' + qs.stringify({
    q: term,
    startIndex: offset,
    maxResults: 10,
  }));
  // TODO: better error handling
  if (res.status >= 400) return [];
  const body = await res.json();
  return body.items.map(parse);
}

export async function getById(id: string): Promise<?Book> {
  const res = await fetch(`https://www.googleapis.com/books/v1/volumes/${id}`);
  const body = await res.json();
  // TOOD: better error handling
  if (res.status >= 400) return null;
  return parse(body);
}
