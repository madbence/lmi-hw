import Inferno from 'inferno';
import Component from 'inferno-component';
import {connect} from 'inferno-redux';
import {Link} from 'inferno-router';

import * as books from './api';
import {receive, clear} from './actions';

import * as cart from '../cart/api';
import {add, remove} from '../cart/actions';

class List extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: false,
      loadingNext: false,
      page: 0,
    };
  }

  componentDidMount() {
    if (!this.props.params.q) {
      this.context.router.push('/?q=javascript');
      return;
    }
    this.search(this.props.params.q);
  }

  componentWillReceiveProps(next) {
    if (this.props.params.q === next.params.q) return;
    this.search(next.params.q);
  }

  async search(term) {
    this.setState({loading: true});
    await this.props.search(term, 0);
    this.setState({loading: false});
  }

  async next() {
    if (this.state.loadingNext) return;
    this.setState({loadingNext: true});
    await this.props.search(this.props.params.q, (this.state.page + 1) * 10);
    this.setState({loadingNext: false, page: this.state.page + 1});
  }

  render() {
    const props = this.props;
    const state = this.state;
    const add = id => e => {
      e.stopPropagation();
      e.preventDefault();
      this.props.add(id);
    };
    const remove = id => e => {
      e.stopPropagation();
      this.props.remove(id);
    };
    return (
      <div className='book-list'>
        <ul className='book-list--results'>
          {
            state.loading
              ? <li>Loading...</li>
              : props.books.length === 0
                ? <li>No results...</li>
                : [...props.books.map(book => (
                  <li className='book-list-item'>
                    <div className='book-list-item--thumbnail'>
                      <img src={book.thumbnail} alt={book.title} />
                    </div>
                    <div className='book-list-item--info'>
                      <Link to={`/books/${book.id}`}>
                        <h2 className='book-list-item--title'>{book.title}</h2>
                      </Link>
                      {book.subtitle && <h3 className='book-list-item--subtitle'>{book.subtitle}</h3>}
                      {book.authors.length && <h3 className='book-list-item--authors'>by {book.authors.join(', ')}</h3>}
                      {book.publisher && <h3 className='book-list-item--publisher'>Publisher: {book.publisher}, {book.publishDate}</h3>}
                      {book.price && <h3 className='book-list-item--price'>Price: {book.price} {book.currency}</h3>}
                      {book.added ? <button onClick={remove(book.id)}>remove</button> : <button onClick={add(book.id)}>add</button>}
                    </div>
                  </li>
                )), <button className='book-list--more' onClick={() => this.next()}>{this.state.loadingNext ? 'Loading...' : 'Load more'}</button>]
          }
        </ul>
      </div>
    );
  }
}

export default connect(state => ({
  books: Object.values(state.books.byId).map(book => ({
    ...book,
    added: state.cart.includes(book.id),
  })),
}), dispatch => ({
  async search(term, offset) {
    if (offset === 0) dispatch(clear());
    const items = await books.search(term, offset);
    dispatch(receive(items));
  },
  async add(id) {
    await cart.add(id);
    dispatch(add(id));
  },
  async remove(id) {
    await cart.remove(id);
    dispatch(remove(id));
  },
}))(List);
