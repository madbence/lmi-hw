import http from 'http';
import fs from 'fs';
import url from 'url';

const serve = (res, file, type) => {
  res.writeHead(200, {'Content-Type': type});
  res.end(fs.readFileSync(file));
};

http.createServer((req, res) => {
  const parsed = url.parse(req.url);
  switch (parsed.path) {
    case '/logmein-bookstore/bundle.js': return serve(res, './dist/bundle.js', 'application/javascript');
    case '/logmein-bookstore/bundle.css': return serve(res, './dist/bundle.css', 'text/css');
    default: return serve(res, './dist/index.html', 'text/html');
  }
}).listen(3000);
