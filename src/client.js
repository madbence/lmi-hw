import 'babel-polyfill';
import Inferno from 'inferno';
import {Router, Route} from 'inferno-router';
import createBrowserHistory from 'history/createBrowserHistory';
import {Provider} from 'inferno-redux';
import store from './store';

import App from './app';
import BookList from './book/list';
import BookDetails from './book/details';
import Cart from './cart';

const history = createBrowserHistory({basename: '/logmein-bookstore'});

Inferno.render(
  <Provider store={store}>
    <Router history={history}>
      <Route component={(props) => <App {...props} term={props.params.q} />}>
        <Route path='/' component={BookList} />
        <Route path='/cart' component={Cart} />
        <Route path='/books/:id' component={({params}) => <BookDetails id={params.id} />} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
