export const ADD = 'cart:add';
export const REMOVE = 'cart:remove';

export const add = id => ({
  type: ADD, payload: id,
});

export const remove = id => ({
  type: REMOVE, payload: id,
});
