import Inferno from 'inferno';
import Component from 'inferno-component';
import {connect} from 'inferno-redux';
import {Link} from 'inferno-router';

import * as cart from './api';
import {remove} from './actions';

import * as books from '../book/api';
import {receive} from '../book/actions';

class Cart extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: false,
    };
  }

  async componentDidMount() {
    if (this.props.items.length === this.props.ids.length) return;
    this.setState({loading: true});
    await this.props.load(this.props.ids.filter(id => !this.props.items.find(item => item.id === id)));
    this.setState({loading: false});
  }

  render() {
    const props = this.props;
    const state = this.state;
    return (
      <div className='cart'>
        {
          state.loading
            ? <h1>Loading...</h1>
            : props.items.length < 1
              ? <h1>No items in your cart.</h1>
              : props.items.map(item => (
                <div className='cart--item'>
                  <img src={item.thumbnail} className='cart--item-thumbnail' />
                  <div className='cart--item-info'>
                    <Link className='cart--item-title' to={`/books/${item.id}`}>{item.title}</Link>
                    <button className='cart--item-remove' onClick={() => props.remove(item.id)}>Remove from cart</button>
                  </div>
                </div>
              ))
        }
      </div>
    );
  }
}

export default connect(state => ({
  items: state.cart.map(id => state.books.byId[id]).filter(Boolean),
  ids: state.cart,
}), dispatch => ({
  async remove(id) {
    await cart.remove(id);
    dispatch(remove(id));
  },
  async load(ids) {
    const items = [];
    for (const id of ids) items.push(await books.getById(id));
    dispatch(receive(items));
  },
}))(Cart);
