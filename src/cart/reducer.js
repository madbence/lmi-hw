import {ADD, REMOVE} from './actions';

export default (state, action) => {
  switch (action.type) {
    case ADD:
      return {
        ...state,
        cart: [...state.cart, action.payload],
      };
    case REMOVE:
      return {
        ...state,
        cart: state.cart.filter(id => id !== action.payload),
      };
    default:
      return state;
  }
};
