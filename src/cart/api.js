// eslint-disable-next-line require-await
export async function load() {
  return JSON.parse(window.localStorage.getItem('cart'));
}

// eslint-disable-next-line require-await
export async function add(id) {
  const cart = JSON.parse(window.localStorage.getItem('cart') || '[]');
  window.localStorage.setItem('cart', JSON.stringify([...cart, id]));
}

// eslint-disable-next-line require-await
export async function remove(id) {
  const cart = JSON.parse(window.localStorage.getItem('cart') || '[]');
  window.localStorage.setItem('cart', JSON.stringify(cart.filter(id_ => id !== id_)));
}
