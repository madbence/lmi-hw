#!/bin/bash

origin=$(git remote get-url origin)
cd dist
cp index.html 404.html
rm -rf .git
git init
git remote add origin $origin
git checkout -b gh-pages
git add -f bundle.js bundle.css index.html 404.html
git commit -m 'chore: publish gh-pages'
git push -f origin gh-pages
