# logmein-bookstore

## requirements

- `node@8` (a recent one)
- `npm@5`
- `make`

## usage

```sh
$ npm i        # to install dependencies
$ make watch   # for development (local server on port 3000)
$ make test    # to run tests (make sure that the dev server is running!)
$ make build   # for production bundles
$ make publish # to publish gh-pages
```

## notes

- in production, a proper backend should be used :)
- bundles should be versioned (instead of `bundle.js`, `bundle.{version}.js`)
- unit tests should be added (not just integration tests)
- serviceworkers should be added for offline support
- images should be lazyly loaded (an they should be resized to minimize network traffic)
- book management should be optimized (search results can be cached)
- integration tests should be repeatable (more rubust), sometimes they fail because of browser timings
- error handling should be more detailed (eg. show error message instead of showing no results)
- client error reporting (eg. sentry) should be added
- analytics & other services should be added (google analytics, intercom, etc.)
