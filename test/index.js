/* eslint-env mocha */
import assert from 'assert';
import puppeteer from 'puppeteer';

describe('logmein-bookstore', () => {
  it('should work', async () => {
    const browser = await puppeteer.launch({args: ['--no-sandbox']});
    const page = await browser.newPage();

    // load main page
    await page.goto('http://localhost:3000/logmein-bookstore/');
    await page.screenshot({path: 'test-01.png'});

    // wait for search results
    await page.waitForSelector('.book-list-item');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    await page.screenshot({path: 'test-02.png'});

    // goto details page
    await page.click('.book-list-item a');
    await page.screenshot({path: 'test-03.png'});

    // add to cart
    assert.equal(await page.evaluate(() => document.querySelector('.header--cart-container a').innerText), 'Cart');
    await page.click('.book-details button');
    await page.screenshot({path: 'test-04.png'});
    assert.equal(await page.evaluate(() => document.querySelector('.header--cart-container a').innerText), 'Cart (1)');

    // remove from cart
    await page.click('.book-details button');
    assert.equal(await page.evaluate(() => document.querySelector('.header--cart-container a').innerText), 'Cart');

    // goto cart page
    await page.click('.book-details button');
    await page.click('.header--cart-container a');
    await page.waitForSelector('.cart--item');
    await page.screenshot({path: 'test-05.png'});

    // reload page
    await page.reload();
    await page.waitForSelector('.cart--item');

    // remove from cart
    await page.click('.cart--item-remove');
    await page.screenshot({path: 'test-06.png'});
    assert.equal(await page.evaluate(() => document.querySelector('.cart h1').innerText), 'No items in your cart.');

    // search
    await page.click('.header--search');
    for (let i = 0; i < 'javascript'.length; i++) await page.press('Delete');
    await page.type('Game of Thrones');
    await page.click('.header--search-button');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    assert.equal(await page.evaluate(() => document.querySelectorAll('.book-list-item').length), 10);
    await page.screenshot({path: 'test-07.png'});

    // load next page
    await page.click('.book-list--more');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    assert.equal(await page.evaluate(() => document.querySelectorAll('.book-list-item').length), 20);
  });
});
