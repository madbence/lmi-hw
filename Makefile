BIN = node_modules/.bin

build: dist/bundle.js dist/bundle.css dist/index.html

publish: clean build
	./publish.sh

clean:
	rm -rf dist/*

test:
	$(BIN)/eslint 'src/**/*.js'
	$(BIN)/flow
	rm -f *.png
	$(BIN)/mocha --compilers js:babel-register -r babel-polyfill -t 0

dist/index.html: src/index.html
	cp src/index.html dist/index.html

dist/bundle.js: dist/bundle.prod.js
	$(BIN)/uglifyjs --compress --mangle --output dist/bundle.js -- dist/bundle.prod.js

dist/bundle.prod.js: src/**/*.js
	NODE_ENV=production $(BIN)/browserify -d -t babelify -o dist/bundle.prod.js src/client.js

dist/bundle.css: src/styles/*.styl
	$(BIN)/stylus -c -o dist/bundle.css src/styles/app.styl

watch:
	cp src/index.html dist
	$(BIN)/stylus -w -o dist/bundle.css src/styles/app.styl & \
	$(BIN)/watchify -d -v -t babelify -o dist/bundle.js src/client.js & \
	$(BIN)/babel-node src/server
	wait

.PHONY: watch build clean test
